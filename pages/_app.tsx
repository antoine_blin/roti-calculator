import Head from "next/head";
import getConfig from "next/config";
import "../styles/globals.scss";
import cssVariables from "../styles/variables.module.scss";
import { createMuiTheme, ThemeProvider } from "@material-ui/core";

function MyApp({ Component, pageProps }) {
  const { publicRuntimeConfig } = getConfig();
  const PREFIX = publicRuntimeConfig.prefix;

  // list of fonts used in scss to be preloaded (most common format type)
  const fontsToPreload = ["Roboto-Thin.woff"];

  // "cssVariables" is the list of variables set and exported in scss file "variables.module.scss"
  // so it can be accessible from scss files and react components by the material ui theme
  const theme = createMuiTheme({
    palette: {
      primary: {
        main: cssVariables.colPrimary,
      },
      secondary: {
        main: cssVariables.colSecondary,
      },
    },
  });

  return (
    <>
      <Head>
        <title>ROTI Calculator</title>
        <link rel="icon" href={PREFIX + "/favicon.png"} />
        {fontsToPreload.map((font, index) => {
          // preload fonts
          return (
            <link
              key={index}
              rel="preload"
              href={`${PREFIX}/fonts/${font}`}
              as="font"
              crossOrigin=""
            />
          );
        })}
        <meta name="viewport" content="width=device-width, user-scalable=no" />
      </Head>
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </>
  );
}

export default MyApp;
