import { Button, makeStyles, Theme } from "@material-ui/core";
import commonStyles from "../styles/VoteGrid.module.scss";

export interface VoteGridProps {
  onVote: (voteValue: number) => void;
}

const VOTE_VALUES = [1, 2, 3, 4, 5];

const useStyles = makeStyles((theme: Theme) => ({
  voteButton: {
    backgroundColor: theme.palette.secondary.main,
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: 0,
    boxShadow: "none",
    height: "30vh",
    fontSize: "6vh",
    fontFamily: "Roboto-Thin",
  },
}));

const VoteGrid = ({ onVote }: VoteGridProps) => {
  const classes = useStyles();
  return (
    <div className={commonStyles.container}>
      {VOTE_VALUES.map((voteValue) => (
        <Button
          key={voteValue}
          className={classes.voteButton}
          onClick={() => onVote(voteValue)}
        >
          {voteValue}
        </Button>
      ))}
    </div>
  );
};

export default VoteGrid;
