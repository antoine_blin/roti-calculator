import commonStyles from "../styles/VotesDisplay.module.scss";

export interface VotesDisplayProps {
  votes: Array<number>;
}

const VotesDisplay = ({ votes }: VotesDisplayProps) => {
  return (
    <div className={commonStyles.container}>
      {votes.map((vote) => (
        <div key={vote} className={commonStyles.voteValue}>
          {vote}
        </div>
      ))}
    </div>
  );
};

export default VotesDisplay;
