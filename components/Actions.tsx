import { IconButton, makeStyles, Theme } from "@material-ui/core";
import { DeleteOutline, History } from "@material-ui/icons";
import commonStyles from "../styles/Actions.module.scss";

export interface ActionsProps {
  onBack: () => void;
  onClear: () => void;
}

const useStyles = makeStyles((theme: Theme) => ({
  actionButton: {
    backgroundColor: theme.palette.secondary.main,
    border: `1px solid ${theme.palette.divider}`,
    width: "50%",
    borderRadius: 0,
    boxShadow: "none",
    fontSize: "6vh",
    fontFamily: "Roboto-Thin",
  },
}));

const Actions = ({ onBack, onClear }: ActionsProps) => {
  const classes = useStyles();
  return (
    <div className={commonStyles.container}>
      <IconButton
        className={classes.actionButton}
        aria-label="back"
        onClick={onBack}
      >
        <History />
      </IconButton>
      <IconButton
        className={classes.actionButton}
        aria-label="back"
        onClick={onClear}
      >
        <DeleteOutline />
      </IconButton>
    </div>
  );
};

export default Actions;
